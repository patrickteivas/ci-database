CREATE TABLE Users (
    UserID int,
    FirstName varchar(255),
    LastName varchar(255)
);

INSERT INTO Users (UserID, FirstName, LastName) VALUES (1, "TestFirst1", "TestLast1");
INSERT INTO Users (UserID, FirstName, LastName) VALUES (2, "TestFirst2", "TestLast2");
INSERT INTO Users (UserID, FirstName, LastName) VALUES (3, "TestFirst3", "TestLast3");
INSERT INTO Users (UserID, FirstName, LastName) VALUES (4, "TestFirst4", "TestLast4");
INSERT INTO Users (UserID, FirstName, LastName) VALUES (5, "TestFirst5", "TestLast5");
INSERT INTO Users (UserID, FirstName, LastName) VALUES (6, "TestFirst6", "TestLast6");
INSERT INTO Users (UserID, FirstName, LastName) VALUES (7, "TestFirst7", "TestLast7");
INSERT INTO Users (UserID, FirstName, LastName) VALUES (8, "TestFirst8", "TestLast8");
INSERT INTO Users (UserID, FirstName, LastName) VALUES (9, "TestFirst9", "TestLast9");
INSERT INTO Users (UserID, FirstName, LastName) VALUES (10, "TestFirst10", "TestLast10");
