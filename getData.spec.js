const getData = require("./getData");

describe("Get data from database", () => {
	it("User with firstname 'asd' does not exist", (done) => {
		getData("asd", (result) => {
            expect(result).toMatchSnapshot();
            done()
        })
	});
	it("User with firstname 'TestFirst1' exists", (done) => {
		getData("TestFirst1", (result) => {
            expect(result).toMatchSnapshot();
            done()
        })
	});
});
